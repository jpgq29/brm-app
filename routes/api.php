<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/roles', 'RolesController@getAll')->name('roles.add');
Route::post('/roles', 'RolesController@add')->name('roles.save');
Route::patch('/roles/edit', 'RolesController@edit')->name('roles.update');
Route::post('/roles/delete', 'RolesController@delete')->name('roles.delete');
