@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Página principal') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <div class="alert alert-info" role="alert">
                        {{ __('Hola '). Auth::user()->name. (', usted tiene los roles de: "').$roles.'"' }}
                      </div>

                      <div class="alert alert-warning" role="alert">
                        {{ ('Este formulario solo puede ser visto por roles Admin y User')}}
                      </div>

                    <example-component></example-component>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
