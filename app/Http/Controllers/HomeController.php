<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['admin', 'user']);

        $arrRoles = DB::select("select b.name from role_user a join roles b on(b.id = a.role_id) where a.user_id = ".Auth::user()->id);

        $arrRolesName = [];
        foreach($arrRoles as $item){
            $arrRolesName[] = $item->name;
        }

        $rolesPorComas = implode(",",$arrRolesName);

        return view('home', ['roles' => $rolesPorComas]);
    }
}
