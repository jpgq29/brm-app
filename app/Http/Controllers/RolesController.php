<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Auth;
use DB;

class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function getAll()
    {
        $customers = Role::toBase()->get();
        return response()->json([
            'data' => $customers
        ]);
    }

    public function add(Request $request)
    {
        $rules = [
            'name' => 'required',
            'description' => 'required'
        ];

        $errorMessage = [
            'required' => 'Debe ingresar el campo :attribute .'
        ];

        $this->validate($request, $rules, $errorMessage);



        Role::create([
            'name' => $request->name,
            'description' => $request->description,
        ]);

        return response()->json([
            'mensaje' => 'El rol ha sido creado'
        ]);
    }

    public function edit(Request $request)
    {
        $rules = [
            'id' => 'required',
            'name' => 'required',
            'description' => 'required'
        ];

        $errorMessage = [
            'required' => 'Debe ingresar el campo :attribute .'
        ];

        $this->validate($request, $rules, $errorMessage);


        $affected = DB::table('roles')
            ->where('id', $request->id)
            ->update([
                'name' => $request->name,
                'description' => $request->description,
            ]);

        return response()->json([
            'mensaje' => 'El rol ha sido actualizado'
        ]);
    }


    public function delete(Request $request)
    {
        $rules = [
            'id' => 'required',
        ];

        $errorMessage = [
            'required' => 'Debe ingresar el campo :attribute .'
        ];

        $this->validate($request, $rules, $errorMessage);

        DB::table('roles')->where('id', $request->id)->delete();

        return response()->json([
            'mensaje' => 'El rol ha sido eliminado'
        ]);
    }
}
